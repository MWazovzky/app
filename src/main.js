import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import vuetify from "@/plugins/vuetify"
import VueTheMask from "vue-the-mask"
import VueHttp from "@/plugins/http"
import VueGoogleMaps from "@/plugins/google-maps"

Vue.use(VueTheMask)
Vue.use(VueHttp)
Vue.use(VueGoogleMaps)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount("#app")
