import UserIndex from "../views/User/Index.vue"
import UserShow from "../views/User/Show.vue"
import UserEdit from "../views/User/Edit.vue"
import UserCreate from "../views/User/Create.vue"

const routes = [
  {
    path: "/user",
    name: "UserIndex",
    component: UserIndex,
    props: route => ({
      role: route.query.role,
      roles: route.query.roles,
    }),
    meta: { isAdmin: true },
  },
  {
    path: "/user/create",
    name: "UserCreate",
    component: UserCreate,
    meta: { isAdmin: true },
  },
  {
    path: "/user/:user_id",
    name: "UserShow",
    component: UserShow,
    props: true,
    meta: { isOwner: true },
  },
  {
    path: "/user/:user_id/edit",
    name: "UserEdit",
    component: UserEdit,
    props: true,
    meta: { isOwner: true },
  },
]

export default routes
