const routes = [
  {
    path: "/register",
    name: "Register",
    component: () => import(/* webpackChunkName: "register" */ "../views/Auth/Register.vue"),
    meta: { layout: "Auth" },
  },
  {
    path: "/login",
    name: "Login",
    component: () => import(/* webpackChunkName: "login" */ "../views/Auth/Login.vue"),
    meta: { layout: "Auth" },
  },
  {
    path: "/reset-password",
    name: "ResetPassword",
    component: () => import(/* webpackChunkName: "login" */ "../views/Auth/ResetPassword.vue"),
    meta: { layout: "Auth" },
  },
]

export default routes
