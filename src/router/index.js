import Vue from "vue"
import VueRouter from "vue-router"
import store from "@/store"
import auth from "./auth"
import users from "./users"
import posts from "./posts"
import Home from "../views/Home.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/contacts",
    name: "Contacts",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/Contacts.vue"),
  },
  ...auth,
  ...users,
  ...posts,
  {
    path: "/page-not-found",
    alias: "*",
    name: "Error404",
    component: () => import(/* webpackChunkName: "login" */ "../views/Error/404.vue"),
    meta: { layout: "Error" },
  },
]

const router = new VueRouter({
  mode: "history",
  routes,
})

router.beforeEach((to, from, next) => {
  const isAdmin = store.getters["auth/isAdmin"]
  const isOwner = store.getters["auth/isOwner"](to.params["user_id"])
  const isAuthenticated = store.getters["auth/isAuthenticated"]

  if (to.matched.some(record => record.meta.isAdmin) && !isAdmin) {
    store.dispatch("snackbar/show", { text: "Access denied" })
    return
  }

  if (to.matched.some(record => record.meta.isOwner) && !(isOwner || isAdmin)) {
    store.dispatch("snackbar/show", { text: "Access denied" })
    return
  }

  if (to.matched.some(record => record.meta.isAuthenticated) && !isAuthenticated) {
    store.dispatch("snackbar/show", { text: "Access denied" })
    return
  }

  next()
})

export default router
