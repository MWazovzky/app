import CategoryIndex from "../views/Category/Index.vue"
import TagIndex from "../views/Tag/Index.vue"
import PostIndex from "../views/Post/Index.vue"
import PostShow from "../views/Post/Show.vue"
import PostCreate from "../views/Post/Create.vue"
import PostEdit from "../views/Post/Edit.vue"

const routes = [
  {
    path: "/categories",
    name: "CategoryIndex",
    component: CategoryIndex,
  },
  {
    path: "/tags",
    name: "TagIndex",
    component: TagIndex,
  },
  {
    path: "/posts",
    name: "PostIndex",
    component: PostIndex,
    props: route => ({
      search: route.query.search,
      category: route.query.category,
      tags: route.query.tags,
      favorite: route.query.favorite == true || route.query.favorite == "true",
      orderby: route.query.orderby,
    }),
  },
  {
    path: "/posts/:id",
    name: "PostShow",
    component: PostShow,
    props: true,
  },
  {
    path: "/posts/create",
    name: "PostCreate",
    component: PostCreate,
    meta: { isAuthenticated: true },
  },
  {
    path: "/posts/:id/edit",
    name: "PostEdit",
    component: PostEdit,
    props: true,
    meta: { isAuthenticated: true },
  },
]

export default routes
