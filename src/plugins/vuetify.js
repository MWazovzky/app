import Vue from "vue"
import Vuetify from "vuetify/lib"

Vue.use(Vuetify)

const options = {
  theme: {
    themes: {
      dark: {
        primary: "#21CFF3",
        accent: "#FF4081",
        secondary: "#ffe18d",
      },
      light: {
        primary: "#19568A",
        accent: "#ff5451",
        secondary: "#3a4750",
        background: "#f8f8f8",
      },
    },
  },
}

export default new Vuetify(options)
