import http from "@/services/http"

export default {
  install(Vue) {
    Vue.prototype.$http = http
  },
}
