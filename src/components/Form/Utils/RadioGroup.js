import { VRadioGroup } from "vuetify/lib"

export default {
  extends: VRadioGroup,

  methods: {
    getValue(item) {
      return item.value
    },
  },
}
