import UserCard from "./Card"
import UserFilters from "./Filters"
import UserForm from "./Form"
import UserPhones from "./Phones"
import UserPhoto from "./Photo"

const API_URL = "users"

const ITEM_DEFAULT = {
  name: "",
  email: "",
  role_id: null,
  photo: null,
}

export { API_URL, ITEM_DEFAULT, UserCard, UserFilters, UserForm, UserPhones, UserPhoto }
