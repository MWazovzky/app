import PostCard from "./Card"
import PostFilters from "./Filters"
import PostForm from "./Form"
import PostPhotos from "./Photos"
import PostPhoto from "./Photo"
import PostFavorite from "./Favorite"

const API_URL = "posts"

const ITEM_DEFAULT = {
  title: "",
  body: "",
  category_id: null,
  tags: [],
}

export {
  API_URL,
  ITEM_DEFAULT,
  PostCard,
  PostFilters,
  PostForm,
  PostPhotos,
  PostPhoto,
  PostFavorite,
}
