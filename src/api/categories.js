import http from "@/services/http"

const API_URL = "categories"

function tree() {
  return http.get(`${API_URL}/tree`)
}

function createNode(node) {
  return http.post(`${API_URL}`, node)
}

function updateNode(id, form) {
  return http.put(`${API_URL}/${id}`, form)
}

function destroyNode(id) {
  return http.delete(`${API_URL}/${id}`)
}

export default {
  tree,
  createNode,
  updateNode,
  destroyNode,
}
