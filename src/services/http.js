import axios from "axios"
import store from "@/store"
import router from "@/router"

const http = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
})

http.interceptors.request.use(
  config => {
    const token = store.getters["auth/apiToken"]
    config.headers["Authorization"] = `Bearer ${token}`
    return config
  },
  error => {
    return Promise.reject(error)
  },
)

http.interceptors.response.use(
  response => {
    return response
  },
  error => {
    switch (error.response.status) {
      case 401:
        store.dispatch("snackbar/show", { text: "Access denied" })
        store.dispatch("auth/logout")
        router.push({ name: "Home" })
        break
      case 403:
        store.dispatch("snackbar/show", { text: "Access denied" })
        break
      case 404:
        store.dispatch("snackbar/show", { text: "Object not found" })
        break
      case 500:
        store.dispatch("snackbar/show", { text: "Server error" })
        break
    }
    return Promise.reject(error)
  },
)

export default http
