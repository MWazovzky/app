export default {
  namespaced: true,

  state: {
    show: false,
    color: "error",
    text: "",
    timeout: 5000,
  },

  getters: {
    GET: state => state,
  },

  mutations: {
    SET(state, { show = true, color = "error", text = "" }) {
      state.show = show
      state.color = color
      state.text = text
    },
  },

  actions: {
    show({ commit }, { text, color = "error" }) {
      commit("SET", { show: true, color, text })
    },
  },
}
