import { shallowMount, createLocalVue } from "@vue/test-utils"
import VueRouter from "vue-router"
import Login from "@/views/Auth/Login.vue"

const localVue = createLocalVue()
localVue.use(VueRouter)

describe("Login.vue", () => {
  it("renders title and inputs", () => {
    const wrapper = shallowMount(Login, { localVue })
    expect(wrapper.text()).toMatch("Login")
  })
})
